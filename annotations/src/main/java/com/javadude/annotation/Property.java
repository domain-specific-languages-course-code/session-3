package com.javadude.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface Property {
    String name();
    Class<?> type();
}
