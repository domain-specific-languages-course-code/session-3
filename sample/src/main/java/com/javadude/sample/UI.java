package com.javadude.sample;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class UI extends JFrame {
    private JButton addButton;
    private JButton removeButton;

    public static void main(String[] args) throws Throwable {
        new UI();
    }

    public UI() throws Throwable {
        setLayout(new FlowLayout());
        addButton = new JButton("Add");
        removeButton = new JButton("Remove");
        add(addButton);
        add(removeButton);
        UISetup.setup(this);
//        addButton.addActionListener(this::addPressed);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    @Action("addButton")
    public void addPressed(ActionEvent e){
        System.out.println("Add pressed");
    }
    @Action("removeButton")
    public void removePressed(ActionEvent e){
        System.out.println("Remove pressed");
    }
}
