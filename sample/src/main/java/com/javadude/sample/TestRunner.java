package com.javadude.sample;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TestRunner {
    static class TestToRun {
        Method method;
        int order;

        public TestToRun(Method method, int order) {
            this.method = method;
            this.order = order;
        }
    }
    public static void main(String[] args) throws Throwable {
        String className = args[0];
        Class<?> clazz = Class.forName(className);
        Object o = clazz.newInstance();
        List<TestToRun> tests = new ArrayList<>();
        for (Method method : clazz.getMethods()) {
            Test test = method.getAnnotation(Test.class);
            if (test != null) {
                tests.add(new TestToRun(method, test.order()));
            }
        }
        tests.sort(new Comparator<TestToRun>() {
            @Override
            public int compare(TestToRun o1, TestToRun o2) {
                if (o1.order < o2.order)
                    return -1;
                if (o1.order > o2.order)
                    return 1;
                return 0;
            }
        });
        for (TestToRun testToRun : tests) {
            testToRun.method.invoke(o);
        }
    }
}
