package com.javadude.sample;

import com.javadude.annotation.Bean;
import com.javadude.annotation.Property;

@Bean(properties = {
        @Property(name = "name", type = String.class),
        @Property(name = "age", type = int.class)
})
public class Person extends PersonGen {
    public void foo() {
        System.out.println(getName());
    }
}
