package com.javadude.sample;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class UISetup {
    public static void setup(Object o) throws Throwable {
        Class<?> c = o.getClass();
        for (Method method : c.getMethods()) {
            Action action = method.getAnnotation(Action.class);
            if (action != null) {
                String fieldName = action.value();
                Field field = c.getDeclaredField(fieldName);
                field.setAccessible(true);
                JButton button = (JButton) field.get(o);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            method.invoke(o, e);
                        } catch (IllegalAccessException | InvocationTargetException e1) {
                            throw new RuntimeException(e1);
                        }
                    }
                });
            }
        }
    }
}
