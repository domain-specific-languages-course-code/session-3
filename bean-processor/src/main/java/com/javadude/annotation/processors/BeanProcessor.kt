package com.javadude.annotation.processors

import com.javadude.annotation.Bean
import com.javadude.annotation.Property
import java.util.*
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.PackageElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.MirroredTypeException
import javax.tools.Diagnostic

class BeanProcessor : AbstractProcessor() {
    private fun String.toFirstUpper() =
        this[0].toUpperCase() + this.substring(1)

    private fun Array<Property>.gen() =
            this.joinToString("\n\n") {
                """    private ${it.typeName()} ${it.name};
                  |    public ${it.typeName()} get${it.name.toFirstUpper()}() {
                  |        return ${it.name};
                  |    }
                  |    public void set${it.name.toFirstUpper()}(${it.typeName()} value) {
                  |        this.${it.name} = value;
                  |    }
                """.trimMargin()
            }

    private fun Property.typeName() =
        try {
            this.type.qualifiedName
        } catch (e : MirroredTypeException) {
            e.typeMirror.toString()
        }

    override fun process(set: Set<TypeElement>, roundEnv: RoundEnvironment): Boolean {
        for (element in roundEnv.getElementsAnnotatedWith(Bean::class.java)) {
            val bean = element.getAnnotation(Bean::class.java)
            val packageName = (element.enclosingElement as PackageElement).qualifiedName
            val code =
                    """package $packageName;
                      |
                      |public class ${element.simpleName}Gen {
                      |${bean.properties.gen()}
                      |}
                    """.trimMargin()
            processingEnv.messager.printMessage(Diagnostic.Kind.ERROR, "", element)
            val sourceFile = processingEnv.filer.createSourceFile("$packageName.${element.simpleName}Gen", element)
            sourceFile.openWriter().use {
                it.write(code)
            }
        }
        return true
    }

    override fun getSupportedAnnotationTypes(): Set<String> {
        val set = HashSet<String>()
        set.add(Bean::class.java.name)
        set.add(Property::class.java.name)
        return set
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.RELEASE_8
    }
}
